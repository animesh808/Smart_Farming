package com.example.kamru.mqttchatapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static Button publishButton;
    private static EditText messageEdit;
    private static TextView messageText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sendMessage();
    }

    public void sendMessage(){
        publishButton = findViewById(R.id.button_publish);
        messageEdit = findViewById(R.id.editText);
        messageText = findViewById(R.id.msgText);

        publishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                messageText.setText(messageEdit.getText());
                messageEdit.getText().clear();
            }
        });

    }
}
